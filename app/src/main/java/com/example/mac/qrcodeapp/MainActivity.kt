package com.example.mac.qrcodeapp

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.google.zxing.integration.android.IntentIntegrator
import io.fabric.sdk.android.Fabric

import kotlinx.android.synthetic.main.activity_main.*

/**
 INI MASIH DI MASTER
 */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener(View.OnClickListener {
            goScan()
        })

        val mainFragment = MainFragment()
        var mainFM = supportFragmentManager.beginTransaction().replace(R.id.content_main, mainFragment)
        mainFM.commit()
    }

    fun goHome(v : View) {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    private fun goScan() {
        var integrator = IntentIntegrator(this)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
        integrator.setPrompt("Scan")
        integrator.setCameraId(0)
        integrator.setBeepEnabled(false)
        integrator.setBarcodeImageEnabled(false)
        integrator.initiateScan()
        integrator.setOrientationLocked(false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        var result = IntentIntegrator.parseActivityResult(resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "You're Cancelling", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, result.contents, Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

}
