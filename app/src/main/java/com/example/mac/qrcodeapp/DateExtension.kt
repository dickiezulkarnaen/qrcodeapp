package com.example.mac.qrcodeapp

import java.text.SimpleDateFormat
import java.util.*

const val REGISTER_DATE_FORMAT = "yyyy-MM-dd"

internal fun getCurrentDay(): Int = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

internal fun getCurrentMonth(): Int = Calendar.getInstance().get(Calendar.MONTH)

internal fun getCurrentYear(): Int = Calendar.getInstance().get(Calendar.YEAR)

internal fun getCurrentDate(): String {
    val dateTime = SimpleDateFormat(REGISTER_DATE_FORMAT, Locale.ENGLISH)
    return dateTime.format(Date().time)
}