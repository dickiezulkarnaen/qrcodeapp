package com.example.mac.qrcodeapp


import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main.view.*
import java.util.*

class MainFragment : Fragment() {
    var startDay = getCurrentDay()
    var startMonth = getCurrentMonth()
    var startYear = getCurrentYear()

    val startDatePickerListener = DatePickerDialog.OnDateSetListener{ view, year, monthOfYear, dayOfMonth ->
        this.tv_start_date.text = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth
        this.tv_end_date.text = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth
        startDay = dayOfMonth
        startMonth = monthOfYear
        startYear = year
    }

    val endDatePickerListener = DatePickerDialog.OnDateSetListener{ view, year, monthOfYear, dayOfMonth ->
        this.tv_end_date.text = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_main, container, false)
        view.tv_start_date.text = getCurrentDate()
        view.tv_end_date.text = getCurrentDate()
        view.tv_start_date.setOnClickListener(View.OnClickListener {
            setStartDateDialog()
        })
        view.tv_end_date.setOnClickListener(View.OnClickListener {
            setEndDateDialog()
        })
        return view
    }

    @SuppressLint("NewApi")
    fun setStartDateDialog() {
        var datePickerDialog = DatePickerDialog(context, startDatePickerListener, getCurrentYear(), getCurrentMonth(), getCurrentDay())
        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()+(1000*60*60*24*6)
        datePickerDialog.show()
    }

    @SuppressLint("NewApi")
    fun setEndDateDialog() {
        var datePickerDialog = DatePickerDialog(context, endDatePickerListener, startYear, startMonth, startDay)
        val startDate = Calendar.getInstance()
        startDate.set(startYear, startMonth, startDay)
        datePickerDialog.datePicker.minDate = startDate.timeInMillis
        datePickerDialog.datePicker.maxDate = startDate.timeInMillis+(1000*60*60*24*6)
        datePickerDialog.show()
    }

}
